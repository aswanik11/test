[{
	"id": "uuid-1",
	"name": "Webprovise Corp",
	"cost": 52983,
	"parentEmployee": [{
		"id": "uuid-t1",
		"name": "Garry Schuppe",
		"cost": "362.00"
	}, {
		"id": "uuid-t3",
		"name": "Cheyenne Turcotte",
		"cost": "859.00"
	}],
	"childCompany": [{
		"id": "uuid-2",
		"name": "Stamm LLC",
		"cost": 1429,
		"parentId": "uuid-1",
		"childEmployee": [{
			"id": "uuid-t2",
			"name": "Alison Kohler Sr.",
			"cost": "835.00"
		}, {
			"id": "uuid-t4",
			"name": "Marielle Bartoletti",
			"cost": "191.00"
		}, {
			"id": "uuid-t5",
			"name": "Dejah Ullrich MD",
			"cost": "403.00"
		}],
		"subChildCompany": [{
			"id": "uuid-4",
			"name": "Price and Sons",
			"cost": 1340,
			"parentId": "uuid-2",
			"subChildEmployee": [{
				"id": "uuid-t13",
				"name": "Rocio Ziemann",
				"cost": "137.00"
			}, {
				"id": "uuid-t14",
				"name": "Mr. Adela Casper",
				"cost": "761.00"
			}, {
				"id": "uuid-t15",
				"name": "Ms. Elijah Crooks",
				"cost": "442.00"
			}],
			"subSubChildCompany": []
		}, {
			"id": "uuid-7",
			"name": "Zieme - Mills",
			"cost": 1636,
			"parentId": "uuid-2",
			"subChildEmployee": [{
				"id": "uuid-t22",
				"name": "Mrs. Darryl Kilback",
				"cost": "955.00"
			}, {
				"id": "uuid-t23",
				"name": "Lillie Skiles",
				"cost": "431.00"
			}, {
				"id": "uuid-t24",
				"name": "Zelda Kuhlman",
				"cost": "201.00"
			}, {
				"id": "uuid-t25",
				"name": "Marshall Hand",
				"cost": "49.00"
			}],
			"subSubChildCompany": []
		}, {
			"id": "uuid-19",
			"name": "Schneider - Adams",
			"cost": 794,
			"parentId": "uuid-2",
			"subChildEmployee": [{
				"id": "uuid-t91",
				"name": "Mr. Gerda Dietrich",
				"cost": "685.00"
			}, {
				"id": "uuid-t92",
				"name": "Ezra Huels",
				"cost": "109.00"
			}],
			"subSubChildCompany": []
		}]
	}, {
		"id": "uuid-3",
		"name": "Blanda, Langosh and Barton",
		"cost": 3847,
		"parentId": "uuid-1",
		"childEmployee": [{
			"id": "uuid-t6",
			"name": "Danial Barrows V",
			"cost": "383.00"
		}, {
			"id": "uuid-t7",
			"name": "Mrs. Jettie Effertz",
			"cost": "104.00"
		}, {
			"id": "uuid-t8",
			"name": "Seamus Reynolds",
			"cost": "698.00"
		}, {
			"id": "uuid-t9",
			"name": "Mittie Vandervort",
			"cost": "500.00"
		}, {
			"id": "uuid-t10",
			"name": "Mayra Pouros",
			"cost": "393.00"
		}, {
			"id": "uuid-t11",
			"name": "Pete Orn",
			"cost": "999.00"
		}, {
			"id": "uuid-t12",
			"name": "Selina Dibbert",
			"cost": "770.00"
		}],
		"subChildCompany": [{
			"id": "uuid-5",
			"name": "Hane - Windler",
			"cost": 1288,
			"parentId": "uuid-3",
			"subChildEmployee": [{
				"id": "uuid-t16",
				"name": "Thad Schaefer",
				"cost": "234.00"
			}, {
				"id": "uuid-t17",
				"name": "Rudy Langosh",
				"cost": "477.00"
			}, {
				"id": "uuid-t18",
				"name": "Miss Jacques Dickinson",
				"cost": "577.00"
			}],
			"subSubChildCompany": []
		}, {
			"id": "uuid-6",
			"name": "Vandervort - Bechtelar",
			"cost": 2512,
			"parentId": "uuid-3",
			"subChildEmployee": [{
				"id": "uuid-t19",
				"name": "Harry Rohan",
				"cost": "872.00"
			}, {
				"id": "uuid-t20",
				"name": "Rebeca Breitenberg",
				"cost": "735.00"
			}, {
				"id": "uuid-t21",
				"name": "Misael Jones V",
				"cost": "905.00"
			}],
			"subSubChildCompany": []
		}, {
			"id": "uuid-9",
			"name": "Kuhic - Swift",
			"cost": 3086,
			"parentId": "uuid-3",
			"subChildEmployee": [{
				"id": "uuid-t31",
				"name": "Mrs. Zion Parisian",
				"cost": "906.00"
			}, {
				"id": "uuid-t32",
				"name": "Greg Ernser",
				"cost": "774.00"
			}, {
				"id": "uuid-t33",
				"name": "Marcos Thompson",
				"cost": "932.00"
			}, {
				"id": "uuid-t34",
				"name": "Katrina Konopelski",
				"cost": "474.00"
			}],
			"subSubChildCompany": []
		}, {
			"id": "uuid-17",
			"name": "Rohan, Mayer and Haley",
			"cost": 4072,
			"parentId": "uuid-3",
			"subChildEmployee": [{
				"id": "uuid-t82",
				"name": "Citlalli Zieme",
				"cost": "228.00"
			}, {
				"id": "uuid-t83",
				"name": "Uriah Glover",
				"cost": "738.00"
			}, {
				"id": "uuid-t84",
				"name": "Rosanna Macejkovic",
				"cost": "401.00"
			}, {
				"id": "uuid-t85",
				"name": "Ms. Sylvan Flatley",
				"cost": "654.00"
			}, {
				"id": "uuid-t86",
				"name": "Miguel Hegmann",
				"cost": "439.00"
			}, {
				"id": "uuid-t87",
				"name": "Leta Rowe",
				"cost": "887.00"
			}, {
				"id": "uuid-t88",
				"name": "Syble Ondricka",
				"cost": "157.00"
			}, {
				"id": "uuid-t89",
				"name": "Mrs. Jerod Rowe",
				"cost": "568.00"
			}],
			"subSubChildCompany": []
		}, {
			"id": "uuid-20",
			"name": "Kunde, Armstrong and Hermann",
			"cost": 908,
			"parentId": "uuid-3",
			"subChildEmployee": [{
				"id": "uuid-t94",
				"name": "Alana Predovic",
				"cost": "622.00"
			}, {
				"id": "uuid-t95",
				"name": "Nathanael O'Hara",
				"cost": "217.00"
			}, {
				"id": "uuid-t96",
				"name": "Myah Doyle DVM",
				"cost": "69.00"
			}],
			"subSubChildCompany": []
		}]
	}, {
		"id": "uuid-8",
		"name": "Bartell - Mosciski",
		"cost": 2605,
		"parentId": "uuid-1",
		"childEmployee": [{
			"id": "uuid-t26",
			"name": "Dr. Eudora Stroman",
			"cost": "32.00"
		}, {
			"id": "uuid-t27",
			"name": "Keara Trantow",
			"cost": "484.00"
		}, {
			"id": "uuid-t28",
			"name": "Chadd Lebsack",
			"cost": "146.00"
		}, {
			"id": "uuid-t29",
			"name": "Rachael Bauch",
			"cost": "740.00"
		}, {
			"id": "uuid-t30",
			"name": "Alana Hettinger",
			"cost": "689.00"
		}, {
			"id": "uuid-t53",
			"name": "Mireille Boyer",
			"cost": "71.00"
		}, {
			"id": "uuid-t54",
			"name": "Maude Bergstrom",
			"cost": "443.00"
		}],
		"subChildCompany": [{
			"id": "uuid-10",
			"name": "Lockman Inc",
			"cost": 4288,
			"parentId": "uuid-8",
			"subChildEmployee": [{
				"id": "uuid-t35",
				"name": "Newton Homenick",
				"cost": "315.00"
			}, {
				"id": "uuid-t36",
				"name": "Hollie Langosh PhD",
				"cost": "970.00"
			}, {
				"id": "uuid-t37",
				"name": "Nathanial Medhurst",
				"cost": "870.00"
			}, {
				"id": "uuid-t38",
				"name": "Quinten Reilly",
				"cost": "486.00"
			}, {
				"id": "uuid-t39",
				"name": "Laisha Kihn",
				"cost": "501.00"
			}, {
				"id": "uuid-t50",
				"name": "Norma Olson IV",
				"cost": "38.00"
			}, {
				"id": "uuid-t51",
				"name": "Lucas Jacobson",
				"cost": "469.00"
			}, {
				"id": "uuid-t52",
				"name": "Miss Isom Stokes",
				"cost": "639.00"
			}],
			"subSubChildCompany": []
		}, {
			"id": "uuid-11",
			"name": "Parker - Shanahan",
			"cost": 2872,
			"parentId": "uuid-8",
			"subChildEmployee": [{
				"id": "uuid-t40",
				"name": "Candida Little",
				"cost": "705.00"
			}, {
				"id": "uuid-t41",
				"name": "Judson Rowe",
				"cost": "889.00"
			}, {
				"id": "uuid-t42",
				"name": "Antonia Nitzsche",
				"cost": "873.00"
			}, {
				"id": "uuid-t43",
				"name": "Brenna Fadel",
				"cost": "405.00"
			}],
			"subSubChildCompany": [{
				"id": "uuid-12",
				"name": "Swaniawski Inc",
				"cost": 2110,
				"parentId": "uuid-11",
				"subSubChildEmployee": [{
					"id": "uuid-t44",
					"name": "Salvatore Anderson",
					"cost": "978.00"
				}, {
					"id": "uuid-t45",
					"name": "Zack Barrows II",
					"cost": "716.00"
				}, {
					"id": "uuid-t46",
					"name": "Louie Gusikowski",
					"cost": "416.00"
				}]
			}, {
				"id": "uuid-14",
				"name": "Weimann, Runolfsson and Hand",
				"cost": 7254,
				"parentId": "uuid-11",
				"subSubChildEmployee": [{
					"id": "uuid-t55",
					"name": "Boyd Romaguera",
					"cost": "674.00"
				}, {
					"id": "uuid-t56",
					"name": "Otha Crona",
					"cost": "274.00"
				}, {
					"id": "uuid-t57",
					"name": "Cedrick Howe",
					"cost": "818.00"
				}, {
					"id": "uuid-t58",
					"name": "Jovany Sporer MD",
					"cost": "848.00"
				}, {
					"id": "uuid-t59",
					"name": "Jeromy Carter",
					"cost": "666.00"
				}, {
					"id": "uuid-t60",
					"name": "Aidan Kutch",
					"cost": "186.00"
				}, {
					"id": "uuid-t61",
					"name": "Hayden Collins",
					"cost": "609.00"
				}, {
					"id": "uuid-t62",
					"name": "Nigel Hilll",
					"cost": "493.00"
				}, {
					"id": "uuid-t63",
					"name": "Kamryn Quitzon",
					"cost": "68.00"
				}, {
					"id": "uuid-t64",
					"name": "Dusty Harvey V",
					"cost": "212.00"
				}, {
					"id": "uuid-t65",
					"name": "Raoul D'Amore",
					"cost": "472.00"
				}, {
					"id": "uuid-t66",
					"name": "Jaclyn Gutkowski",
					"cost": "419.00"
				}, {
					"id": "uuid-t67",
					"name": "Chandler Ondricka",
					"cost": "603.00"
				}, {
					"id": "uuid-t68",
					"name": "Magali Cummerata",
					"cost": "781.00"
				}, {
					"id": "uuid-t69",
					"name": "Aubrey Kozey",
					"cost": "131.00"
				}]
			}]
		}, {
			"id": "uuid-13",
			"name": "Balistreri - Bruen",
			"cost": 1686,
			"parentId": "uuid-8",
			"subChildEmployee": [{
				"id": "uuid-t47",
				"name": "Nona Jerde MD",
				"cost": "577.00"
			}, {
				"id": "uuid-t48",
				"name": "Edd Johnson",
				"cost": "237.00"
			}, {
				"id": "uuid-t49",
				"name": "Danielle Hettinger",
				"cost": "872.00"
			}],
			"subSubChildCompany": []
		}, {
			"id": "uuid-15",
			"name": "Predovic and Sons",
			"cost": 4725,
			"parentId": "uuid-8",
			"subChildEmployee": [{
				"id": "uuid-t70",
				"name": "Ms. Concepcion Treutel",
				"cost": "941.00"
			}, {
				"id": "uuid-t71",
				"name": "Joan Kertzmann",
				"cost": "845.00"
			}, {
				"id": "uuid-t72",
				"name": "Sherman Ward",
				"cost": "999.00"
			}, {
				"id": "uuid-t73",
				"name": "Fannie Bosco",
				"cost": "954.00"
			}, {
				"id": "uuid-t74",
				"name": "Hyman D'Amore",
				"cost": "923.00"
			}, {
				"id": "uuid-t75",
				"name": "Gust Ruecker",
				"cost": "63.00"
			}],
			"subSubChildCompany": []
		}, {
			"id": "uuid-16",
			"name": "Weissnat - Murazik",
			"cost": 3277,
			"parentId": "uuid-8",
			"subChildEmployee": [{
				"id": "uuid-t76",
				"name": "Giovanna Johnston",
				"cost": "53.00"
			}, {
				"id": "uuid-t77",
				"name": "Wiley Kihn",
				"cost": "718.00"
			}, {
				"id": "uuid-t78",
				"name": "Mia Lowe",
				"cost": "691.00"
			}, {
				"id": "uuid-t79",
				"name": "Garry Treutel",
				"cost": "482.00"
			}, {
				"id": "uuid-t80",
				"name": "Cyril Sipes",
				"cost": "812.00"
			}, {
				"id": "uuid-t81",
				"name": "Jameson Berge",
				"cost": "521.00"
			}],
			"subSubChildCompany": []
		}]
	}, {
		"id": "uuid-18",
		"name": "Walter, Schmidt and Osinski",
		"cost": 2033,
		"parentId": "uuid-1",
		"childEmployee": [{
			"id": "uuid-t90",
			"name": "Cortney VonRueden",
			"cost": "100.00"
		}, {
			"id": "uuid-t93",
			"name": "Steve Schiller",
			"cost": "44.00"
		}, {
			"id": "uuid-t97",
			"name": "Ms. Percy Herzog",
			"cost": "652.00"
		}, {
			"id": "uuid-t98",
			"name": "Mariam Volkman",
			"cost": "817.00"
		}, {
			"id": "uuid-t99",
			"name": "Annamarie Cormier",
			"cost": "119.00"
		}, {
			"id": "uuid-t100",
			"name": "Walter Gerhold",
			"cost": "301.00"
		}],
		"subChildCompany": []
	}]
}]