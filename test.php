<?php
class Travel
{
    function travelData()
    {
        $url = "https://5f27781bf5d27e001612e057.mockapi.io/webprovise/travels";
        $travel_data = curl_init();
        curl_setopt($travel_data, CURLOPT_URL, $url);
        curl_setopt($travel_data, CURLOPT_RETURNTRANSFER, true);
        $result =  curl_exec($travel_data);
        return $result;
    }
}


class Company
{
    // Enter your code here
    function companyData()
    {
        $url = "https://5f27781bf5d27e001612e057.mockapi.io/webprovise/companies";
        $company_data = curl_init();
        curl_setopt($company_data, CURLOPT_URL, $url);
        // curl_setopt($travel_data, CURLOPT_GET, true);
        curl_setopt($company_data, CURLOPT_RETURNTRANSFER, true);
        $result =  curl_exec($company_data);
        return $result;
    }
}

class TestScript
{
    public function execute()
    {
        $start = microtime(true);
        // Enter your code here
        $company_obj =  new Company();
        $arrayData = $company_obj->companyData();
        $arrayData = json_decode($arrayData);


        $travel_obj = new Travel();
        $arrayTravelData = $travel_obj->travelData();
        $arrayTravelData = json_decode($arrayTravelData);

        $final_arr = array();
        $total_cost = 0;
        $parentCompany = array();
        $childCompany = array();
        //get company list having no parent company

        foreach ($arrayData as  $value) {
            if ($value->parentId == "0") {

                array_push($parentCompany, $value->id);
            }
        }

        // get child company list
        foreach ($arrayData as  $value) {
            if (in_array($value->parentId, $parentCompany)) {
                //get sub child company list
                $subChildCompany = array();
                foreach ($arrayData as  $v) {
                    if ($v->parentId == $value->id) {
                        // get sub child company employee
                        // get child company employee
                        $subChildEmployee = array();
                        $total_cost_sub_child = 0;
                        foreach ($arrayTravelData as $t_v) {
                            if ($v->id === $t_v->companyId) {
                                $subChildEmployeeObj = array(
                                    "id" => $t_v->id,
                                    'name' => $t_v->employeeName,
                                    'cost' => $t_v->price,
                                );
                                array_push($subChildEmployee, $subChildEmployeeObj);
                                $total_cost = $total_cost + $t_v->price;
                                $total_cost_sub_child = $total_cost_sub_child + $t_v->price;
                            }
                        }
                        // print_r($subChildEmployee);
                        // die;

                        /*get sub sub child company  list start*/
                        $subSubChildCompany = array();
                        foreach ($arrayData as  $v1) {
                            if ($v1->parentId == $v->id) {
                                // get sub sub child company employee
                                $subSubChildEmployee = array();
                                $total_cost_sub_sub_child = 0;
                                foreach ($arrayTravelData as $t_v1) {
                                    if ($v1->id === $t_v1->companyId) {
                                        $subSubChildEmployeeObj = array(
                                            "id" => $t_v1->id,
                                            'name' => $t_v1->employeeName,
                                            'cost' => $t_v1->price,
                                        );
                                        array_push($subSubChildEmployee, $subSubChildEmployeeObj);
                                        $total_cost = $total_cost + $t_v1->price;
                                        $total_cost_sub_sub_child = $total_cost_sub_sub_child + $t_v1->price;
                                    }
                                }
                                // print_r($subChildEmployee);
                                // die;

                                $subSubChildCompanyObj = array(
                                    'id' => $v1->id,
                                    'name' => $v1->name,
                                    'cost' => $total_cost_sub_sub_child,
                                    'parentId' => $v1->parentId,
                                    'subSubChildEmployee' => $subSubChildEmployee
                                );
                                array_push($subSubChildCompany, $subSubChildCompanyObj);
                            }
                        }
                        /*get sub sub child company  list end*/

                        $subChildCompanyObj = array(
                            'id' => $v->id,
                            'name' => $v->name,
                            'cost' => $total_cost_sub_child,
                            'parentId' => $v->parentId,
                            'subChildEmployee' => $subChildEmployee,
                            'subSubChildCompany' => $subSubChildCompany
                        );
                        array_push($subChildCompany, $subChildCompanyObj);
                    }
                }
                // get child company employee
                $childEmployee = array();
                $total_cost_child = 0;
                foreach ($arrayTravelData as $t_value) {
                    if ($value->id === $t_value->companyId) {
                        $childEmployeeObj = array(
                            "id" => $t_value->id,
                            'name' => $t_value->employeeName,
                            'cost' => $t_value->price,
                        );
                        array_push($childEmployee, $childEmployeeObj);
                        $total_cost = $total_cost + $t_value->price;
                        $total_cost_child = $total_cost_child + $t_value->price;
                    }
                }
                // print_r($childEmployee);
                // die;

                $childCompanyObj = array(
                    'id' => $value->id,
                    'name' => $value->name,
                    'cost' => $total_cost_child,
                    'parentId' => $value->parentId,
                    'childEmployee' => $childEmployee,
                    'subChildCompany' => $subChildCompany
                );
                array_push($childCompany, $childCompanyObj);
            }
        }

        // print_r($childCompany);
        // die;

        //final expected data
        foreach ($arrayData as  $value) {
            if (in_array($value->id, $parentCompany)) {
                // get parent company employee
                $parentEmployee = array();
                foreach ($arrayTravelData as $t_value) {
                    if ($value->id === $t_value->companyId) {
                        $parentEmployeeObj = array(
                            "id" => $t_value->id,
                            'name' => $t_value->employeeName,
                            'cost' => $t_value->price,
                        );
                        array_push($parentEmployee, $parentEmployeeObj);
                        $total_cost = $total_cost + $t_value->price;
                    }
                }
                $jsonObj = array(
                    'id' => $value->id,
                    'name' => $value->name,
                    'cost' => $total_cost,
                    'parentEmployee' => $parentEmployee,
                    'childCompany' => $childCompany
                );
                array_push($final_arr, $jsonObj);
            }
        }


        print_r(json_encode($final_arr));


        // echo json_encode($result);
        echo 'Total time: ' .  (microtime(true) - $start);
    }
}

(new TestScript())->execute();
