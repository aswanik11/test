<?php
class Travel
{
    function travelData()
    {
        $url = "https://5f27781bf5d27e001612e057.mockapi.io/webprovise/travels";
        $travel_data = curl_init();
        curl_setopt($travel_data, CURLOPT_URL, $url);
        curl_setopt($travel_data, CURLOPT_RETURNTRANSFER, true);
        $result =  curl_exec($travel_data);
        return $result;
    }
}


class Company
{
    // Enter your code here
    function companyData()
    {
        $url = "https://5f27781bf5d27e001612e057.mockapi.io/webprovise/companies";
        $company_data = curl_init();
        curl_setopt($company_data, CURLOPT_URL, $url);
        // curl_setopt($travel_data, CURLOPT_GET, true);
        curl_setopt($company_data, CURLOPT_RETURNTRANSFER, true);
        $result =  curl_exec($company_data);
        return $result;
    }
}

class TestScript
{
    public function execute()
    {
        $start = microtime(true);
        // Enter your code here
        $company_obj =  new Company();
        $arrayData = $company_obj->companyData();
        $arrayData = json_decode($arrayData);


        $travel_obj = new Travel();
        $arrayData1 = $travel_obj->travelData();
        $arrayData1 = json_decode($arrayData1);

        $final_arr = array();
        $total_cost = 0;
        foreach ($arrayData as  $value) {
            $child_arr = array();
            foreach ($arrayData1 as $value1) {

                if ($value->id === $value1->companyId) {
                    $childObjs = array(
                        "id" => $value1->id,
                        'name' => $value1->employeeName,
                        'cost' => $value1->price,

                    );
                    array_push($child_arr, $childObjs);
                    $total_cost = $total_cost + $value1->price;
                }
            }
            $jsonObj = array(
                'id' => $value->id,
                'name' => $value->name,
                'cost' => $total_cost,
                'children' => $child_arr
            );
            array_push($final_arr, $jsonObj);
        }

        print_r(json_encode($final_arr));


        // echo json_encode($result);
        echo 'Total time: ' .  (microtime(true) - $start);
    }
}

(new TestScript())->execute();
